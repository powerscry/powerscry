import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense


"""
Data Preparation:
First we prepare our data.
The X_train contains the features (numerical card representations)
The Y_train contains the labels (card strength ratings)
"""

# Model Configuration
model = Sequential([ # initializing a Sequential model
    Dense(units=25, activation='sigmoid', input_shape=(X_train.shape[1],)),
    Dense(units=15, activation='sigmoid'),
    Dense(units=10, activation='softmax')
])

# Model Compilation
model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3), # Using Adam optimizer with a learning rate of 0.001
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True), # Using sparse categorical crossentropy loss for classification. This is another variant of the cross-entropy loss function used for multi-class classification tasks
    metrics=['accuracy']  # Monitoring accuracy as a metric
)

# Model Training
model.fit(X_train, y_train, epochs=100) # This will train the model with the trainings data for 100 loops