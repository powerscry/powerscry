import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from sklearn.preprocessing import StandardScaler
import pandas as pd

# Define the neural network architecture. This consists out of 3 components: neurons, layers & weights
model = Sequential([
    Dense(units=25, activation='sigmoid', input_shape=(number_of_features,)),
    Dense(units=15, activation='sigmoid'),
    Dense(units=1, activation='linear') 
])

# load dataset
data = pd.read_csv('...')

# This splits the dataset into features and target variable
X = data.drop('ability', axis=1) # This extracts features by dropping the column 'ability'
y = data['ability'] # This extracts the target column ('ability')

# This will standardize the features. That means that it will make the values of eact feature in the data have zero-meaning.
scaler = StandardScaler() # This will initialize a StandardScaler object for feature scaling. Here are the values centererd around the mean.
X_scaled = scaler.fit_transform(X)  # This will scale the features using the StandardScaler object

# Train the model
model.fit(X_scaled, y, epochs=100)  # This will train the model using the features and target for 100 loops