import numpy as np

# This is the Regularization of the strength, This will control the impact of the regularization term. Regularization will redcude overfitting in machine learning models
lambda_ = 0.1
# This is the initial weight for the model, in other words the starting value
weights = 1
# This will initial the bias for the model, randomly generated
bias = np.random.randn()

# Calculates mean sqared error between predictions and targets.
def loss_function(predictions, targets):
    return np.mean((predictions - targets) ** 2)

# This calculates the L2 regularization term for the weights.
def regularization(weights, lambda_):
    return lambda_ * np.sum(weights ** 2)

# This evaluates the model using the loss function and regularization.
def evaluate_model(features, targets, weights, bias, lambda_):
    predictions = features.dot(weights) + bias # Compute predictions
    loss = loss_function(predictions, targets) # Calculate loss, 
    reg = regularization(weights, lambda_) # Calculate regularization
    total_loss = loss + reg # Total loss is sum of loss and regularization
    return total_loss