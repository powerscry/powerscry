import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
import json
import random

# Function to process a single feature set
def process_single_feature(feature):
    return np.array(list(map(float, feature.split(',')))).reshape(1, -1)

# Define the prediction function
def predict_ability_rating(ability_key, feature_index=0):
    # Load and parse the JSON features file
    Abilitylink = "AI\\abilities\\Abilitiesrandom.json"
    with open(Abilitylink) as fileC:
        data = json.load(fileC)

    if ability_key not in data:
         random_rating = random.randint(7, 8)
         return random_rating
        #raise ValueError(f"Ability key '{ability_key}' not found in the data.")

    if feature_index >= len(data[ability_key]):
        raise ValueError(f"Feature index '{feature_index}' is out of range for the ability '{ability_key}'.")

    specific_ability_features = data[ability_key][feature_index]  # Get the specific feature set for the ability

    # Process the specific ability features
    X_predict = process_single_feature(specific_ability_features)
    print("Processed feature set:", X_predict)

    # Load the model and apply the same architecture as during training
    model = Sequential([
        Dense(units=128, activation="relu", input_shape=(X_predict.shape[1],)),
        Dropout(0.5),
        Dense(units=64, activation="relu"),
        Dropout(0.5),
        Dense(units=32, activation="relu"),
        Dropout(0.5),
        Dense(units=10, activation="softmax")  # Output layer for 10 classes
    ])

    # Compile the model (same configuration as during training)
    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),
        loss='categorical_crossentropy',
        metrics=['accuracy']
    )

    # Load the best model weights
    checkpoint_path = 'AI/abilities/best_model.weights.h5'
    model.load_weights(checkpoint_path)

    # Make the prediction
    prediction = model.predict(X_predict)
    print("Prediction output (one-hot):", prediction)

    # Convert the prediction from one-hot encoded to class label
    predicted_class = np.argmax(prediction) + 1  # Adding 1 to match the original label range [1, 10]
    print("Predicted class:", predicted_class)

    return predicted_class

