Sacrifice a land, You draw 2 cards - 6
Pay {3}{B}{B} (where {B} is black mana): Exile one target creature each opponent controls, then draw a card. - 4
Basic landcycling {1} ({1}, Discard this card: Search your library for a basic land card, reveal it, put it into your hand, then shuffle.) - 4
Destroy all enchantments - 9
Sacrifice permanet, target creature gets deathtouch - 5
Pay 1 life, you draw 2 cards - 6
Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.) - 7
Vigilance (Attacking doesn't cause this creature to tap.) - 7
Haste (This creature can attack and {T} as soon as it comes under your control.) - 7
Pay {4}{G}: Sacrifice a creature, then destroy target creature - 4
Flying (this creature can't be blocked except by creatures with flying or reach.) - 7
Destroy target creature, You lose 2 life. - 6
Scry 2, then draw a card. - 7
Hexproof (This permanent can't be the target of spells or abilities your opponents control.) - 8
At the end of your combat step, Untap target creature - 6
{W}, {T}: Tap target creature - 6
{T}: Draw a card, then discard a card - 5
Destroy target creature - 7
Destroy traget artifact, target creature, target enchantment, and target land. - 7
When your creature becomes blocked, defending player must sacrifice a non-land permanent - 8
Counter target multicolored spell - 8
Trample (this creature can deal excess combat damaga to a player, planeswaker or battle it's attacking.) - 7
Destroy target red permanent. - 7
Destroy target land, You gain 2 life. - 8
Untap all creatures you control. - 8
Reach (This creature can block creaturs with flying.) - 7
Enchanted creature has first strike, vigilance, and lifelink - 8
Cycling {2} ({2}, Discard this card: Draw a card.) - 5
Menace (This creature can't be blocked except by two or more creatures.) - 7
Enchanted creature can't be blocked - 8
Target creature gets indestructible until end of turn - 7
Whenever you gain life, target opponent loses that much life - 9
Indestructible (Damage and effects that say "destroy" don’t destroy this.) - 8
Target creature gets hexproof until end of turn - 6
Pay {2}, Gain 3 life - 6
First strike (This creature deals combat damaga before creatures without first strike.) - 6
Ward {2} (Whenever this creature becomes the target of a spell or ability an opponent controls, counter it unless that player pays {2}.) - 7
{T}, untap target creature - 5
{W} You gain 5 life - 8
{G}{G} Give target creature trample until end of turn - 7
At the beginning of end step each player exiles top card of their library - 3
{B} (Where {B} is black mana), Target opponent loses 3 life - 5
{T}, Gain one mana in your commander's colors - 7
Lifelink (Damage dealt by this creature also causes you to gain that much life.) - 7
Double strike (This creature deals both first-strike and regular combat damage.) - 8
{T}, add {2} - 7
Destroy target artifact or enchantment. Create a Junk token - 6
Destroy target artifact you don't control. - 4
Draw a card, You lose 1 life - 6
{T}, Untap target land - 4
Target player discards three cards - 7
When you do damage to a player you take the initiative - 7
{T} artifact, target creature can't be blocked - 9
Whenever a creature you control dies, draw a card. - 6
Annihilator 2  (Whenever this creature attacks, defending player sacrifices 2 permanents.) - 8
As extra cost to cast this card, discard 1 card, then draw 2 cards - 6
{B}. Sacrifice a creature: Destroy target nonblack creature - 7
At the beginning of your upkeep, each player loses 1 life - 6
Whenever an opponent casts a noncreature spell, that palyer loses 2 life and you gain 2 life. - 8
Whenever you deal damage to an opponent, create that many food token - 7
Whenever enchanted creature gets blocked, defending player discards their hand and draw that many cards. - 1
Whenever you cast a sorcery/instant you may look at target opponent's hand - 7
Whenever you attack, you tap 1 pernament that defending player owns - 7
When this creature dies, you draw one card and you get 1 life. - 6
Whenever you discard a card you may cast a spell from your graveyard. - 6
Enchanted creature can't attack or block - 7
Flash (you may cast this spell any time you c.ould cast an instant.) - 8
You gain protection from everything until end of turn - 10
Whenever a creature dies you get life equal to its converted mana cost - 8
Destroy all lands on the battlefield - 10
Whenever a creature you control deals combat damage to an oppenent, create a treasure token - 6
Put target card from your graveyard on top of your library. - 8
Tap target creature. Scry 1, then draw a card. - 6
Storm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.) - 9
{1}, {T}: Add {R}{G}{W}. - 9
Sacrifice a permanent, each opponent discards 1 card and you draw 2 cards. - 7
{1}, {T}: Add {B}{R}. - 8
Exile target creature or enchantment, then return it to the battlefield under its owner's control. - 6
When this card enters the battlefield, target player loses 2 life and you gain 2 life. - 6
Exile target card from a graveyard, then draw card - 8
When this card enters the battlefield, draw a card - 6
When this creature dies, draw a card - 5
Target player draws 2 cards, then discards 2 cards - 6
Tap permanent, target creature gets protection from black - 7
Return all attacking creatures to their owner's hand. - 8
Whenever you cast an noncreature spell, you deal 1 damage to each opponent - 6
Target player loses 4 life, you gain 4 life - 4
This spell deals 3 damage to any target - 7
Enchanted creature has lifelink and deathtouch - 8
Destroy all multicolored creatures - 8
Creatures you control have flying, first strike, vigilance, trample, haste, and protection from black and from red. - 9
Whenever a creature you control dies, draw a card - 8
This card deals 1 damge to each creature and planeswalker card target player controlls. - 7
{U}: This creature gains flying until end of turn. - 6
{T}: Target creature gains indestructible until end of turn. - 8
At the beginning of you upkeep scry 1 - 7
At the end of combat untap all creatures you control - 10
Sacrifice a permanent, draw four cards. - 8
Sacrifice a land, you get 5 mana of any color - 8
Whenever you gain life, each opponent loses 1 life. - 7
When you draw a card, target opponent mills 2 cards - 7
Whenever you cast a spell, each opponent mills 2 cards - 7
Whenever this card enters the battlefield, you gain 4 life - 5
At the beginning of your end step you draw 7 cards - 5
Whenever a creature enters the battlefield each opponent takes 1 damage - 7
Sacrifice an artifact, you deal 2 damage to any target - 7