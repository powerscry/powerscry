from GPT import request_segment  # Importing a function from another module
import requests  # Library to handle HTTP requests
import pandas as pd  # Library for data manipulation and analysis
import numpy as np  # Library for numerical operations
import json  # Library for JSON handling
import os  # Library for interacting with the operating system

# Function to load dummy data from a JSON file named "JJ.json"
def dummydata():
    with open("JJ.json") as data:
        d = json.load(data)  # Load the JSON data
        return d  # Return the loaded data

Moxfieldlink =(f"")  # insert moxfieldlink here

# Function to fetch deck data from a given deck ID using an API call
def fetch_deck_data(deck_id):
    response = requests.get(Moxfieldlink)  # API call to fetch deck data
    print(response)  # Print the response status for debugging
    return response.json()  # Return the response data in JSON format

# Function to extract deck ID from a given URL
def deck_id(url):
    id = url.split('/')[-1]  # Split the URL by '/' and take the last part as the ID
    return id  # Return the extracted ID

# Function to process deck information and create a DataFrame
def process_deck_info(deck_id, deck_name, cards):
    # Initialize a row for the deck with the deck ID, name, and 100 placeholders for cards
    deck_row = [deck_id, deck_name] + [None] * 100
    
    # Fill the row with card IDs and quantities, up to 100 cards
    for i, card_id in enumerate(cards):
        if i < 100:
            deck_row[i+2] = card_id, cards[card_id]['quantity']

    # Define column names for the DataFrame
    column_names = ['Deck_ID', 'Deck_Name'] + [f'card_{i:02d}' for i in range(100)]
    # Create a DataFrame with the deck information
    decks_df = pd.DataFrame([deck_row], columns=column_names)
    return decks_df  # Return the DataFrame

# Function to process card information and create a DataFrame
def process_card_info(main_cards, commander_card):
    cards_data = []  # Initialize an empty list to hold card data

    # Extract commander card information and add it to the list
    commander_card_info = extract_card_info(commander_card)
    cards_data.append(commander_card_info)

    # Extract mainboard card information and add it to the list
    for card_id in main_cards:
        card_info = extract_card_info(main_cards[card_id]['card'])
        cards_data.append(card_info)

    # Create a DataFrame with card information
    cards_df = pd.DataFrame(cards_data, columns=['uniqueCardId', 'name', 'colors', 'type_line', 'oracle_text', 'mana_cost', 'power', 'toughness'])
    # Convert color lists to comma-separated strings
    cards_df['colors'] = cards_df['colors'].apply(lambda x: ', '.join(x) if isinstance(x, list) else x)

    return cards_df  # Return the DataFrame

# Function to process deck data and handle duplicates
def process_deck_data(data, json_path):
    # Extract deck ID, name, mainboard cards, and commander card from the data
    deck_id = data['id']
    deck_name = data['name']
    mainboard_cards = data['boards']['mainboard']['cards']
    commander_card = data['main']
    
    # Process deck information and create a DataFrame
    decks_df = process_deck_info(deck_id, deck_name, mainboard_cards)
    # Load existing deck data from JSON
    decks_json = get_json_data(json_path, 'Decks', 'Deck_ID')
    # Remove duplicates between new and existing data
    decks_df = remove_duplicates_between_dataframes(decks_df, decks_json, 'Deck_ID')
    
    # Process card information and create a DataFrame
    cards_df = process_card_info(mainboard_cards, commander_card)
    return decks_df, cards_df  # Return the DataFrames

# Function to extract specific card information
def extract_card_info(card):
    # Return a list with the extracted information, using default None if not present
    return [
        card.get('uniqueCardId', None),
        card.get('name', None),
        card.get('colors', None),
        card.get('type_line', None),
        card.get('oracle_text', None),
        card.get('mana_cost', None),
        card.get('power', None),
        card.get('toughness', None)
    ]

# Function to write DataFrames to a JSON file
def write_to_json(decks_df, cards_df, abilities_df, filename):
    # Create a dictionary with the data from the DataFrames
    data = {
        'Decks': decks_df.to_dict(orient='records'),
        'Kaarten': cards_df.to_dict(orient='records'),
        'Abilities': abilities_df.to_dict(orient='records')
    }
    # Write the dictionary to a JSON file
    with open(filename, 'w') as f:
        json.dump(data, f, indent=4)

# Function to replace oracle text with ability IDs in the card DataFrame
def replace_oracle_text_with_ability_ids(cards_df, abilities_df):
    cards_df_updated = cards_df.copy()  # Create a copy of the card DataFrame

    # Iterate over each card in the DataFrame
    for index, card_row in cards_df_updated.iterrows():
        unique_card_id = card_row['uniqueCardId']
        # Find abilities for the current card
        abilities_for_card = abilities_df[abilities_df['Card Name'] == card_row['name']]

        ability_ids = []  # Initialize an empty list for ability IDs
        # Iterate over each ability and add its ID to the list
        for _, ability_row in abilities_for_card.iterrows():
            ability_id = ability_row['Ability ID']
            ability_text = ability_row['Ability']
            ability_ids.append(ability_id)

        # Join the ability IDs into a single string
        joined_ability_ids = ', '.join(ability_ids)
        # Replace the oracle text with the joined ability IDs
        cards_df_updated.loc[cards_df_updated['uniqueCardId'] == unique_card_id, 'oracle_text'] = joined_ability_ids

    return cards_df_updated  # Return the updated DataFrame

# Function to format card data for segment requests
def format_card_data_for_segment(row):
    # Format the card data into a tab-separated string
    formatted_data = f"{row['uniqueCardId']}\t{row['name']}\t{row['colors']}\t{row['type_line']}\t{row['oracle_text']}\t{row['mana_cost']}\t{row['power']}\t{row['toughness']}"
    return formatted_data  # Return the formatted string

# Function to fetch ability segments for cards and create a DataFrame
def get_ability_segments(card):
    abilities_data = []  # Initialize an empty list to hold abilities data
    seen_segments = set()  # Initialize an empty set to track seen segments

    # Convert a single card Series to DataFrame if necessary
    if isinstance(card, pd.Series):
        card_df = pd.DataFrame([card])
    else:
        card_df = pd.DataFrame(card)

    # Iterate over each card in the DataFrame
    for index, row in card_df.iterrows():
        print(f'__________START Segment {index} {row["name"]} is begonnen__________')
        # Format the card data for segment request
        formatted_card_data = format_card_data_for_segment(row)
        # Request ability segments using the formatted card data
        ability_segments_response = request_segment(formatted_card_data)
        segment_content = ability_segments_response  # Get the segment content
        abilities = segment_content.split('\n')  # Split the content into abilities

        # Iterate over each ability and add unique abilities to the list
        for i, ability in enumerate(abilities):
            if ability not in seen_segments:
                ability_id = f"{row['uniqueCardId']}_{i}"  # Create a unique ability ID
                abilities_data.append({'Card Name': row['name'], 'Ability ID': ability_id, 'Ability': ability})
                seen_segments.add(ability)  # Mark the ability as seen

    # Create a DataFrame with the abilities data
    return pd.DataFrame(abilities_data, columns=['Card Name', 'Ability ID', 'Ability'])

# Function to load a section of JSON data into a DataFrame
def get_json_data(path, section, identifier):
    if not os.path.exists(path):  # Check if the file exists
        print(f"Bestand '{path}' niet gevonden. Retourneert een lege DataFrame.")
        return pd.DataFrame()  # Return an empty DataFrame if the file doesn't exist

    with open(path, 'r') as f:
        data = json.load(f)  # Load the JSON data

    if section not in data:  # Check if the section exists in the data
        return pd.DataFrame()  # Return an empty DataFrame if the section doesn't exist

    return pd.DataFrame(data[section])  # Return the section data as a DataFrame

# Function to load all sections of a JSON file into a dictionary
def get_all_sections(path):
    if not os.path.exists(path):  # Check if the file exists
        return {}  # Return an empty dictionary if the file doesn't exist

    with open(path, 'r') as f:
        data = json.load(f)  # Load the JSON data

    return data  # Return the loaded data

# Function to remove duplicate rows between two DataFrames based on a specified column
def remove_duplicates_between_dataframes(df1, df2, column):
    duplicates_indices = []  # Initialize an empty list to hold indices of duplicate rows
    # Iterate over each row in the second DataFrame
    for i, row_df2 in df2.iterrows():
        # Iterate over each row in the first DataFrame
        for j, row_df1 in df1.iterrows():
            if row_df1[column] == row_df2[column]:  # Check for duplicates based on the specified column
                duplicates_indices.append(j)  # Add the index of the duplicate row to the list

    return df1.drop(duplicates_indices)  # Drop the duplicate rows from the first DataFrame and return it

# Function to append data to a specified section of a JSON file
def append_to_json(df, json_path, section):
    if not os.path.exists(json_path):  # Check if the JSON file exists
        data = {section: df.to_dict(orient='records')}  # Convert the DataFrame to a dictionary
        # Write the data to a new JSON file
        with open(json_path, 'w') as f:
            json.dump(data, f, indent=4)
        return

    with open(json_path, 'r') as f:
        data = json.load(f)  # Load the existing JSON data

    if section in data:  # Check if the section exists in the data
        existing_data = pd.DataFrame(data[section])  # Load the existing section data into a DataFrame
        df = pd.concat([existing_data, df])  # Concatenate the new data with the existing data
    data[section] = df.to_dict(orient='records')  # Convert the combined data to a dictionary

    # Write the updated data back to the JSON file
    with open(json_path, 'w') as f:
        json.dump(data, f, indent=4)

# Main function to process the entire deck data, handle duplicates, and save to JSON
def main_process(url, json_path, cards_df_updated):
    deck_data = dummydata()  # Load dummy data
    decks_df, cards_df = process_deck_data(deck_data, json_path)  # Process the deck data

    if os.path.exists(json_path):  # Check if the JSON file exists
        data = get_all_sections(json_path)  # Load all sections from the JSON file
        decks_json = pd.DataFrame(data.get('Decks', []))  # Load the 'Decks' section into a DataFrame
        cards_json = pd.DataFrame(data.get('Kaarten', []))  # Load the 'Kaarten' section into a DataFrame
        abilities_json = pd.DataFrame(data.get('Abilities', []))  # Load the 'Abilities' section into a DataFrame

        decks_df = remove_duplicates_between_dataframes(decks_df, decks_json, 'Deck_ID')  # Remove duplicate decks
        cards_df = remove_duplicates_between_dataframes(cards_df, cards_json, 'uniqueCardId')  # Remove duplicate cards
        abilities_df = get_ability_segments(cards_df)  # Get ability segments for the cards

        cards_df_updated = replace_oracle_text_with_ability_ids(cards_df, abilities_df)  # Update card texts with ability IDs

        append_to_json(decks_df, json_path, 'Decks')  # Append deck data to the JSON file
        append_to_json(cards_df_updated, json_path, 'Kaarten')  # Append card data to the JSON file
        append_to_json(abilities_df, json_path, 'Abilities')  # Append ability data to the JSON file
    else:
        abilities_df = get_ability_segments(cards_df)  # Get ability segments for the cards
        cards_df_updated = replace_oracle_text_with_ability_ids(cards_df, abilities_df)  # Update card texts with ability IDs
        write_to_json(decks_df, cards_df_updated, abilities_df, json_path)  # Write all data to a new JSON file