import os  # Library for interacting with the operating system
import json  # Library for JSON handling
from openai import OpenAI  # OpenAI library for interacting with the OpenAI API

# Load API key from an environment variable
api_key = os.getenv('OPENAI_API_KEY')
if api_key is None:
    # Raise an error if the API key is not found
    raise ValueError("No API key found. Please set the OPENAI_API_KEY environment variable.")

# Load content from the JSON configuration file
with open('config.json', 'r') as file:
    # Load the JSON data from the configuration file
    config = json.load(file)
    # Access the first element of the 'content' list in the configuration file
    content = config['content'][0]

# Initialize OpenAI client with the API key
client = OpenAI(api_key=api_key)

# Function to request a segment from the OpenAI API using card data
def request_segment(card):
    # Create a completion request to the OpenAI API
    completion = client.chat.completions.create(
        model='gpt-3.5-turbo',  # Specify the model to use (adjust as necessary)
        messages=[
            {"role": "system", "content": content},  # System message with processing instructions
            {"role": "user", "content": card}  # User message with the card data
        ])
    # Return the processed response from the API
    return completion.choices[0].message.content