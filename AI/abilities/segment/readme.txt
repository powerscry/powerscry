Magic: The Gathering Deck Processing README

Overview
This project processes Magic: The Gathering (MTG) deck data, extracts key information from each card, and formats it into a structured JSON file.
The project involves several Python scripts that interact with the OpenAI API to parse and segment card abilities, and handle data storage and processing.

Project Structure
•	decks.json: Contains the deck and card data along with their abilities.
•	Get_segments.py: Contains functions to interact with the OpenAI API for segmenting card data.
•	GPT.py: Contains functions to request segments from the OpenAI API using the GPT model.
•	gpt2.py: depricated version of GPT.py
•	i2.py: Additional helper functions for processing the data.
•	JJ.json: A JSON file containing sample dummy data for testing.
•	numeric.json: Additional JSON data for processing.
•	numeric.py: Additional helper functions for processing the data.
•	parsermtg.py: Additional helper functions for processing the data.

Environment Variables
The project requires several environment variables to be set for proper operation:
•	OPENAI_API_KEY: Your OpenAI API key.
•	DBUser: Database username.
•	DBPassword: Database password.
•	DBHost: Database host.
•	DBTable: Database table name.
Ensure these variables are set in your environment before running the scripts.

How to Run
0. keep in mind, due to some minor mistake, running the segmenter will make more data points than formulaV3 expects at the moment,
causing it to break. the best way to avoid this is running it in a different branch.

1. Open windows command prompt
2. use the command: cd <the path to the folder containing these files found here> 
3. SET_OPENAI_KEY=<your OpenAI key>
4. all the db login details can be skipped for now as they are nonfunctional.
5. use the command: pip install numpy tensorflow scikit-learn matplotlib pandas openai requests
6. use the command: py i2.py

JSON Structure
The decks.json file contains the following sections:
•	Decks: Contains deck information with deck ID, name, and cards.
•	Kaarten: Contains individual card data including unique ID, name, colors, type, oracle text, mana cost, power, and toughness.
•	Abilities: Contains parsed abilities for each card including card name, ability ID, and ability description.