import os  # Importing the os module for interacting with the operating system
from Get_segments import *  # Importing all functions from Get_segments module

# URL of the deck and path to the JSON file
url = 'https://www.moxfield.com/decks/z0eM0QUSxU6KCKT868Xm6w'
json_path = 'decks.json'

# Fetch and process deck data using the dummydata() function and JSON path
decks_df, cards_df = process_deck_data(dummydata(), json_path)

# Initialize a list to store abilities
abilities_list = []

# Retrieve database credentials from environment variables
# DBUser = os.getenv("DBUser")
# if DBUser is None:
#     # Raise an error if DBUser is not found
#     raise ValueError("No DB User found. Please set DBUser environment variable")

# DBPassword = os.getenv("DBPassword")
# if DBPassword is None:
#     # Raise an error if DBPassword is not found
#     raise ValueError("No DBPassword found. Please set DBPassword environment variable")

# DBHost = os.getenv("DBHost")
# if DBPassword is None:
#     # Raise an error if DBHost is not found
#     raise ValueError("No DBHost found. Please set DBHost environment variable")

# DBTable = os.getenv("DBTable")
# if DBPassword is None:
#     # Raise an error if DBTable is not found
#     raise ValueError("No DBTable found. Please set DBTable environment variable")

# Process each card to get abilities
for i, card in cards_df.iterrows():
    # Get ability segments for the current card
    a = get_ability_segments(card)
    
    # Iterate over each ability segment and add it to the abilities list
    for t, b in a.iterrows():
        abilities_list.append(b)
    
    # Limit the processing to the first 60 cards (0 to 59)
    if i == 59:
        # Create a DataFrame from the list of abilities
        abilities_df = pd.DataFrame(abilities_list, columns=['Card Name', 'Ability ID', 'Ability'])

        # Update cards_df with ability IDs using the abilities DataFrame
        cards_df_updated = replace_oracle_text_with_ability_ids(cards_df, abilities_df)

        # Write the updated decks, cards, and abilities data to the JSON file
        write_to_json(decks_df, cards_df_updated, abilities_df, json_path)

        # Print a success message
        print('De deck, kaarten en abilities zijn toegevoegd aan decks.json')
        
        # Exit the script
        quit()