# from openai import OpenAI
# # OpenAI-Beta: assistants=v2
# client = OpenAI(api_key='sk-proj-7nDdc3U4vZmUn3KBvoVET3BlbkFJ2hAIaMh5ohCCDjsdQxm9')

# model = 'gpt-3.5-turbo' #'gpt-4-1106-preview' #'gpt-3.5-turbo'
# content = "To parse oracle_text into categories Trigger, Effect, and Target for Magic: The Gathering card abilities: Assign each ability only one Trigger, Effect, and/or Target. Remove lines without any of these elements or marked as 'None'. Lines without a Trigger, Effect, or Target should be discarded and not counted. Treat each standalone keyword (e.g., Flying, Vigilance, Haste) as separate abilities, listing each as its own line with format: Trigger: None, Effect: {keyword}, Target: None. Keywords within phrases (e.g., 'gives flying') are part of that phrase, not separate. Cards described as 'land' or 'basic land' are to be simplified to one line indicating the mana color they produce, excluding text in parentheses. Use the format for lands: Trigger: {tap}, Effect: gain mana {color}. For non-lands, use the format: Trigger: {specified trigger}, Effect: {specified effect}, Target: {specified target} (omit 'Target' if empty or 'None'; similarly for 'Trigger' and 'Effect'). If a card has no abilities, reply with: 'Null'. Key terms related to actions or conditions like Etb, Cast, Death, and keywords like Lifegain, Lifeloss, Draw should be correctly categorized into Trigger, Effect, or Target. Only list the keywords for Trigger, Effect, or Target, without additional text."
# def request_segment(card):
#   completion = client.chat.completions.create(
#   model=model,
#   messages=[
#     {
#       "role": "system",
#       "content": content
#     },
#     {"role": "user",

#      "content": card}])
#   return completion.choices[0].message.content