import json
import random
from collections import defaultdict

# Load data from the JJ.json file
with open('JJ.json', 'r') as file:
    jj_data = json.load(file)

# Define mappings
triggers = {
    "etb self": 0,
    "etb other": 1,
    "etb artifact": 2,
    "etb enchantment": 3,
    "landfall": 4,
    "constilation": 5,
    "cast self": 6,
    "cast instant or sorcery": 7,
    "targeted": 8,
    "enrage": 9,
    "attacks": 10,
    "blocks": 11,
    "death this": 12,
    "death other creature": 13,
    "sacrifice": 14,
    "gain life": 15,
    "lose life": 16,
    "draw card": 17,
    "discard card": 18,
    "beginning of untap": 19,
    "beginning of upkeep": 20,
    "beginning of main": 21,
    "beginning of combat": 22,
    "beginning of end step": 23,
    "beginning of draw step": 24,
    "end of combat": 25,
    "end of main": 26,
    "end of draw step": 27,
    "blocked": 28
}

costs = {
    "sacrifice permanent": 29,
    "sacrifice creature": 30,
    "sacrifice artifact": 31,
    "pay blue": 32,
    "pay black": 33,
    "pay red": 34,
    "pay green": 35,
    "pay white": 36,
    "pay uncolored": 37,
    "pay life": 38,
    "discard": 39,
    "discard random": 40,
    "{t}": 41,
    "tap permanent": 42,
    "tap artifact": 43,
    "tap creature": 44
}

targets = {
    "creatures": 45,
    "enchantment": 46,
    "artifact": 47,
    "spell": 48,
    "land": 49,
    "permanent": 50,
    "damaged creatures": 51,
    "all creatures": 52,
    "all enchantments": 53,
    "all artifacts": 54,
    "all non-land permanents": 55,
    "all non-creature permanents": 56,
    "all non-enchantment permanents": 57,
    "all non-artifact permanents": 58,
    "itself": 59,
    "enchanted": 60,
    "green": 61,
    "white": 62,
    "black": 63,
    "blue": 64,
    "red": 65,
    "multicolored": 66,
    "monocolored": 67,
    "non-green": 68,
    "non-white": 69,
    "non-black": 70,
    "non-blue": 71,
    "non-red": 72,
    "non-creature": 73,
    "non-token": 74,
    "non-enchantment": 75,
    "non-artifact": 76,
    "non-land": 77,
    "oponent": 78,
    "each opponent": 79,
    "player": 80,
    "each player": 81,
    "you": 82,
    "defending player": 83,
    "opponent controls": 84,
    "player controls": 85,
    "you control": 86,
    "each opponent controls": 87,
    "each player controls": 88,
    "your": 89,
    "player's": 90,
    "opponents": 91,
    "library": 92,
    "graveyard": 93,
    "exile": 94,
    "hand": 95,
    "battlefield": 96
}

effects = {
    "may": 97,
    "must": 98,
    "draw": 99,
    "scry": 100,
    "surveil": 101,
    "discard random": 102,
    "discard choice": 103,
    "no max handsize": 104,
    "cycling": 105,
    "destroy": 106,
    "sacrifice": 107,
    "exile": 108,
    "exile facedown": 109,
    "counter": 110,
    "damage": 111,
    "life gain": 112,
    "life loss": 113,
    "equip": 114,
    "enchant": 115,
    "attach": 116,
    "gain uncolored mana": 117,
    "add {b}": 118,
    "add {g}": 119,
    "add {w}": 120,
    "add {u}": 121,
    "add {r}": 122,
    "gain mana any color": 123,
    "gain mana any one color": 124,
    "gain mana commander color": 125,
    "take initiative": 126,
    "become monarch": 127,
    "venture dungeon": 128,
    "play extra land": 129,
    "tutor sorcery": 130,
    "tutor instant": 131,
    "tutor enchantment": 132,
    "tutor artifact": 133,
    "tutor island": 134,
    "tutor swamp": 135,
    "tutor plains": 136,
    "tutor mountain": 137,
    "tutor forest": 138,
    "tutor basic land": 139,
    "tutor creature": 140,
    "tutor land": 141,
    "flicker insta": 142,
    "flicker endstep": 143,
    "create food": 144,
    "create treasure": 145,
    "create gold": 146,
    "create junk": 147,
    "tap creature": 148,
    "tap artifact": 149,
    "untap creature": 150,
    "untap artifact": 151,
    "protection from sorcery": 152,
    "protection from artifact": 153,
    "protection from enchantment": 154,
    "protection from red": 155,
    "protection from green": 156,
    "protection from black": 157,
    "protection from white": 158,
    "protection from blue": 159,
    "protection from monocolored": 160,
    "protection from multicolored": 161,
    "protection from creature": 162,
    "protection from everything": 163,
    "unblockable": 164,
    "can't block": 165,
    "regenerate": 166,
    "can't be regenerated": 167,
    "prevent damage": 168,
    "copy": 169,
    "double strike": 170,
    "first strike": 171,
    "flash": 172,
    "flying": 173,
    "haste": 174,
    "hexproof": 175,
    "indestructable": 176,
    "landwalk": 177,
    "lifelink": 178,
    "reach": 179,
    "storm": 180,
    "trample": 181,
    "vigilance": 182,
    "convoke": 183,
    "deathtouch": 184,
    "infect": 185,
    "menace": 186,
    "persist": 187,
    "effect": 188,
    "wither": 189,
    "annihilator": 190,
    "fortell": 191,
    "until end of turn": 192,
    "Toughness": 193,
    "Power": 194
}

# Combine all mappings into a single dictionary
all_mappings = {**triggers, **costs, **targets, **effects}

# Function to parse ability text and generate the count array
def parse_ability(ability_text):
    counts = defaultdict(int)
    ability_parts = ability_text.split(',')
    for part in ability_parts:
        part = part.strip().lower()
        for key, value in all_mappings.items():
            if key in part:
                counts[value] += 1
    return counts

# Generate a deck from JJ.json data
def generate_deck_from_jj(json_data):
    deck = {}
    card_index = 0
    for board_key, board in json_data["boards"].items():
        for card_key, card in board["cards"].items():
            card_data = card["card"]
            card_ability = card_data.get("oracle_text", "")
            counts = parse_ability(card_ability)
            counts_array = [counts[i] for i in range(195)]
            deck[f"card_{card_index:02d}"] = {
                "ability_text": card_ability,
                "counts_array": counts_array
            }
            card_index += 1
    return deck

# Generate deck from JJ.json data
deck_from_jj = generate_deck_from_jj(jj_data)

# Save the deck to numeric.json with counts array in one line
class SingleLineArrayEncoder(json.JSONEncoder):
    def encode(self, obj):
        if isinstance(obj, list):
            return '[' + ', '.join(map(self.encode, obj)) + ']'
        return super(SingleLineArrayEncoder, self).encode(obj)

with open('numeric.json', 'w') as file:
    json.dump(deck_from_jj, file, indent=4, cls=SingleLineArrayEncoder)

print("Deck data from JJ.json has been saved to numeric.json")