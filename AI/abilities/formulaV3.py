import os
import numpy as np
import json
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from tensorflow.keras.utils import to_categorical
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from collections import Counter

# Function to process each card's counts_array
def process_counts_array(counts_array):
    return list(map(float, counts_array))

# Function to process each line of ratings
def process_ratings_line(line):
    rating = int(line.strip().split(" - ")[-1])
    return rating

# Ability JSON file link
Abilitylink = "AI//abilities//segment//numeric.json"

# Read and parse the JSON features file
with open(Abilitylink) as fileC:
    data = json.load(fileC)

# Read and parse the ratings file
with open("AI//abilities//elesnornlevels.text") as fileC:
    ratings_lines = fileC.readlines()

# Extract features and ratings
X_train = []
for key, card_data in data.items():
    counts_array = card_data["counts_array"]
    X_train.append(process_counts_array(counts_array))
y_train = [process_ratings_line(line) for line in ratings_lines]

# Adjust class labels to be in the range [0, 9]
y_train = [label - 1 for label in y_train]

# Check the unique class labels
unique_labels = np.unique(y_train)
print(f"Unique class labels: {unique_labels}")

# Check class distribution
class_counts = Counter(y_train)
print("Class distribution:", class_counts)

# Convert to numpy arrays
X = np.array(X_train)
y = np.array(y_train)

# One-hot encode the labels for multi-class classification
y = to_categorical(y, num_classes=10)

# Debugging: Print shapes to verify
print(f"Shape of X: {X.shape}")
print(f"Shape of y: {y.shape}")

# Regular split without stratification
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

# Feature scaling
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_val = scaler.transform(X_val)

# Simplified Model Configuration
model = Sequential([
    Dense(units=128, activation="relu", input_shape=(X_train.shape[1],)),
    Dropout(0.5),
    Dense(units=64, activation="relu"),
    Dropout(0.5),
    Dense(units=32, activation="relu"),
    Dropout(0.5),
    Dense(units=10, activation="softmax")  # Output layer
])

# Model Compilation
model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),
    loss='categorical_crossentropy',
    metrics=['accuracy']
)

# Check if the best model weights file exists
checkpoint_path = 'AI/abilities/best_model.weights.h5'
if os.path.exists(checkpoint_path):
    print("Loading saved weights...")
    model.load_weights(checkpoint_path)
else:
    print("No saved weights found, starting training from scratch.")

# Learning rate scheduler
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=1e-6)

# Early stopping with increased patience
early_stopping = EarlyStopping(monitor='val_loss', patience=20, restore_best_weights=True)

# Model Checkpoint Callback to save the best model
checkpoint = ModelCheckpoint(checkpoint_path, monitor='val_loss', save_best_only=True, save_weights_only=True)

# Model Training with Early Stopping and Checkpoint
history = model.fit(
    X_train, y_train,
    epochs=100,
    batch_size=64,
    validation_data=(X_val, y_val),
    shuffle=True,
    callbacks=[early_stopping, checkpoint, reduce_lr]
)

# Save the final model weights
model.save_weights('AI/abilities/final_model.weights.h5')

# Model Evaluation
loss, accuracy = model.evaluate(X_val, y_val)

print(f"Validation Loss: {loss}")
print(f"Validation Accuracy: {accuracy}")

# Plot learning curves
plt.figure(figsize=(12, 6))

# Plot training & validation accuracy values
plt.subplot(1, 2, 1)
plt.plot(history.history['accuracy'], label='Train Accuracy')
plt.plot(history.history['val_accuracy'], label='Validation Accuracy')
plt.title('Model accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim(0, 1)  # Set y-axis range from 0 to 1
plt.legend(loc='upper left')
plt.grid(True)

# Plot training & validation loss values
plt.subplot(1, 2, 2)
plt.plot(history.history['loss'], label='Train Loss')
plt.plot(history.history['val_loss'], label='Validation Loss')
plt.title('Model loss')
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.legend(loc='upper left')
plt.grid(True)

plt.tight_layout()
plt.show()

# Load the best model weights
model.load_weights(checkpoint_path)

# Verify if weights are loaded correctly by re-evaluating
loss, accuracy = model.evaluate(X_val, y_val)

print(f"Re-evaluated Validation Loss: {loss}")
print(f"Re-evaluated Validation Accuracy: {accuracy}")