pip install numpy tensorflow scikit-learn matplotlib pandas openai requests

Magic: The Gathering Deck Processing Project
Overview
This project processes Magic: The Gathering (MTG) deck data, extracts key information from each card, and formats it into a structured JSON file.
The project includes several Python scripts to interact with the OpenAI API for parsing and segmenting card abilities,
and handles data storage and processing. Additionally, it identifies synergistic card pairs based on predefined criteria.

Project Structure
•    decks.json: Contains the processed deck and card data along with their abilities.
•    synergy_pairs.json: Contains predefined synergistic card pairs.
•    Get_segments.py: Contains functions to interact with the OpenAI API for segmenting card data.
•    GPT.py: Contains functions to request segments from the OpenAI API using the GPT model.
•    extraction.py: Script to process deck data, extract abilities, and update JSON files.

Environment Variables
The project requires several environment variables to be set for proper operation:

•    OPENAI_API_KEY: Your OpenAI API key.
•    DBUser: Database username.
•    DBPassword: Database password.
•    DBHost: Database host.
•    DBTable: Database table name.
•    Ensure these variables are set in your environment before running the scripts.

How to Run
    run extraction.py

Decks: Contains deck information with deck ID, name, and cards.
Kaarten: Contains individual card data including unique ID, name, colors, type, oracle text, mana cost, power, and toughness.
Abilities: Contains parsed abilities for each card including card name, ability ID, and ability description.

Synergy Pairs
The synergy_pairs.json file contains predefined synergistic card pairs based on certain criteria.