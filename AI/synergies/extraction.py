import requests
import json

# Step 1: Fetch card data from Scryfall API
def fetch_card_data(card_name):
    url = f'/cards/named?fuzzy={card_name}' #insert scryfall api here
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        return None

# Step 2: Extract keywords from card text
def extract_keywords(card_text):
    keywords = [
        "draw", "discard", "when enters the battlefield", "enters the battlefield", "leaves the battlefield", 
        "dies", "gain life", "lose life", "deals combat damage", "deals damage", 
        "attacks", "blocks", "land", "landfall", "whenever a land enters the battlefield",
        "proliferate", "+1/+1 counter", "sacrifice"
    ]
    card_text = card_text.lower()  # Convert text to lowercase for case-insensitive matching
    found_keywords = [keyword for keyword in keywords if keyword in card_text]
    return found_keywords

# Step 3: Synergy detection and in-memory storage
cards = {}  # Dictionary to store card data
synergy_count = 0  # Counter to count the number of synergies

def add_card(card_name):
    card_data = fetch_card_data(card_name)
    if card_data:
        card_name = card_data['name']
        card_text = card_data.get('oracle_text', None)
        if card_text:
            cards[card_name] = card_data  # Store entire card data for detailed output
            print(f"Card '{card_name}' added.")
        else:
            print(f"Card '{card_name}' does not have oracle text and was skipped.")
    else:
        print(f"Card '{card_name}' not found.")

def detect_synergies():
    global synergy_count
    # Load synergy pairs from JSON file
    with open('AI\\synergies\\synergy_pairs.json', 'r') as f:
        synergy_pairs = json.load(f)["synergy_pairs"]
    
    for card1_name, card1_data in cards.items():
        card1_keywords = extract_keywords(card1_data['oracle_text'])
        for card2_name, card2_data in cards.items():
            if card1_name == card2_name:
                continue
            card2_keywords = extract_keywords(card2_data['oracle_text'])
            for keyword1, keyword2 in synergy_pairs:
                if keyword1 in card1_keywords and keyword2 in card2_keywords:
                    synergy_count += 1

def fetch_deck_from_moxfield(deck_id):
    url = f'https://api.moxfield.com/v2/decks/all/{deck_id}'
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        return None

def add_deck_from_moxfield(deck_id):
    deck_data = fetch_deck_from_moxfield(deck_id)
    if deck_data and 'mainboard' in deck_data:
        for card_name in deck_data['mainboard']:
            add_card(card_name)
    else:
        print(f"Deck '{deck_id}' not found or empty.")

# Main function to interact with the user
def main():
    print("Enter the Moxfield deck ID you want to analyze.")
    deck_id = input("Enter deck ID: ")
    add_deck_from_moxfield(deck_id)
    
    print("Detecting synergies...")
    detect_synergies()
    
    print(f"The deck has {synergy_count} synergies.")

if __name__ == "__main__":
    main()