pip install numpy tensorflow scikit-learn matplotlib pandas openai requests

Magic: The Gathering Deck Processing Project
Overview
This project processes Magic: The Gathering (MTG) deck data, extracts key information from each card, and formats it into a structured JSON file.
The project includes several Python scripts to interact with the OpenAI API for parsing and segmenting card abilities,
and handles data storage and processing. Additionally, it identifies synergistic card pairs based on predefined criteria.

Project Structure

Files:
    •   decks.json: Contains the processed deck and card data along with their abilities.
    •   synergy_pairs.json: Contains predefined synergistic card pairs.
    •   JJ.json: A JSON file containing sample dummy data for testing.
    •   Combos.json: Contains predefined card combos.
    •   Combosrandom.json: Contains random features related to card combos.
    •   ComboLevels.txt: Contains levels associated with each card combo.
    •   best_model.weights.h5: Weights for the best-performing model.
    •   final_model.weights.h5: Weights for the final model.

Python Scripts:
    •   ComboAI.py: Script for AI-based analysis of card combos.

Environment Variables
The project requires several environment variables to be set for proper operation:

OPENAI_API_KEY: Your OpenAI API key.
DBUser: Database username.
DBPassword: Database password.
DBHost: Database host.
DBTable: Database table name.
Ensure these variables are set in your environment before running the scripts.

Scripts Description
ComboAI.py
    Script for AI-based analysis of card combos.


How to Run
    run ComboAI.py

python extraction.py
JSON Structure
The decks.json file contains the following sections:

Decks: Contains deck information with deck ID, name, and cards.
Kaarten: Contains individual card data including unique ID, name, colors, type, oracle text, mana cost, power, and toughness.
Abilities: Contains parsed abilities for each card including card name, ability ID, and ability description.

Combo Levels
The ComboLevels.txt file contains levels associated with each card combo.