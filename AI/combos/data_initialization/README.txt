pip install numpy tensorflow scikit-learn matplotlib pandas openai requests

Magic: The Gathering Deck Processing Project
Overview
This project processes Magic: The Gathering (MTG) deck data, extracts key information from each card, and formats it into a structured JSON file.
The project includes several Python scripts to interact with the OpenAI API for parsing and segmenting card abilities,
and handles data storage and processing. Additionally, it identifies synergistic card pairs based on predefined criteria.


Project Structure
Files:

    •   decks.json: Contains the processed deck and card data along with their abilities.
    •   synergy_pairs.json: Contains predefined synergistic card pairs.
    •   JJ.json: A JSON file containing sample dummy data for testing.

Python Scripts:

    •   combos.py: Additional script for processing and handling card combinations.
    •   convert.py: Script for converting data formats.
    •   extract_features.py: Script for extracting features from card data.
    •   randomfeatures.py: Script for generating random features for analysis.

The project requires several environment variables to be set for proper operation:

OPENAI_API_KEY: Your OpenAI API key.
DBUser: Database username.
DBPassword: Database password.
DBHost: Database host.
DBTable: Database table name.
Ensure these variables are set in your environment before running the scripts.

Scripts Description
    combos.py
        Script to handle card combinations and interactions.
    convert.py
        Script to convert data formats as needed.
    extract_features.py
        Script to extract features from card data for analysis.
    randomfeatures.py
        Script to generate random features for analysis and testing.


How to Run
    Run the convert.py

Decks: Contains deck information with deck ID, name, and cards.
Kaarten: Contains individual card data including unique ID, name, colors, type, oracle text, mana cost, power, and toughness.
Abilities: Contains parsed abilities for each card including card name, ability ID, and ability description.