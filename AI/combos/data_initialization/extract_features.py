import json
import requests
import time
import math

# This function will fetch data from the API with a specified offset
def fetch_data(api_url, offset):
    try:
        response = requests.get(f"{api_url}&offset={offset}")
        response.raise_for_status()  # Raise an error for bad responses
        data = response.json()
        return data
    except requests.exceptions.RequestException as e:
        print(f"Error fetching data: {e}")
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON from API: {e}")
        print("Response text:", response.text)
    return None

# This function will collect the features from the API
def collect_features(api_url):
    all_features = []
    
    # Fetch the first page to determine the total count and number of calls
    initial_data = fetch_data(api_url, 0)
    if not initial_data or "count" not in initial_data:
        print("Error fetching initial data or count not found in response.")
        return None

    total_count = initial_data["count"]
    num_calls = math.ceil(total_count / 100)
    print(f"Total features count: {total_count}, Number of calls needed: {num_calls}")

    # Add the features from the first call
    if "results" in initial_data:
        all_features.extend(initial_data["results"])

    for i in range(1, num_calls):
        offset = i * 100
        print(f"Fetching data from API (call {i+1}/{num_calls}, offset {offset})...")
        data = fetch_data(api_url, offset)
        if data and "results" in data:
            all_features.extend(data["results"])
        else:
            print(f"No features found in the response for call {i+1}.")
        if i < num_calls - 1:  # Wait for 10 seconds if not the last call
            print("Waiting for 10 seconds before the next call...")
            time.sleep(10)
    
    # Filter out the description from each feature
    for feature in all_features:
        feature.pop("description", None)
    
    return all_features

# This function will sort the features by their ID
def sort_features_by_id(features):
    return sorted(features, key=lambda x: x["id"])

# This function will write the sorted features to a JSON file
def write_features_to_json(features, output_file_path):
    try:
        with open(output_file_path, 'w') as file:
            json.dump(features, file, indent=4)
        print(f"Features successfully written to {output_file_path}")
    except IOError as e:
        print(f"Error writing to file {output_file_path}: {e}")

# This function will write the names of the features to a text file
def write_names_to_text_file(features, output_file_path):
    header_lines = [
        "Num cards needed",
        "Num prerequisites",
        "Num steps",
        "White mana",
        "Blue mana",
        "Black mana",
        "Red mana",
        "Green mana",
        "Colorless mana"
    ]
    try:
        with open(output_file_path, 'w') as file:
            for line in header_lines:
                file.write(line + "\n")
            for feature in features:
                file.write(f"{feature['name']}\n")
        print(f"Feature names successfully written to {output_file_path}")
    except IOError as e:
        print(f"Error writing to file {output_file_path}: {e}")

if __name__ == "__main__":
    api_url = "/features/?limit=100" #insert commanderspellbooklink here
    json_output_file_path = "AI\\combos\\combo\\sorted_features.json"
    text_output_file_path = "AI\\combos\\combo\\feature_names.txt"

    all_features = collect_features(api_url)
    if all_features:
        sorted_features = sort_features_by_id(all_features)
        write_features_to_json(sorted_features, json_output_file_path)
        write_names_to_text_file(sorted_features, text_output_file_path)
    else:
        print("No features collected from the API.")
