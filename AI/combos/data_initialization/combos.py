import json
import requests

# This function will fetch combo data by sending the converted JSON deck data to the Commander Spellbook API.
def fetch_combo_data(deck_data):
    headers = {'Content-Type': 'application/json'}
    spellbookapi=("") #insert commanderspellbookapi here, make sure it's set to json
    try:
        response = requests.post("https://backend.commanderspellbook.com/find-my-combos?format=json", headers=headers, data=json.dumps(deck_data))
        response.raise_for_status()  # Raise an error for bad responses
        combo_data = response.json()
        return combo_data
    except requests.exceptions.RequestException as e:
        print(f"Error fetching combo data: {e}")
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON from Commander Spellbook API: {e}")
        print("Response text:", response.text)
    return None

# This function will read the deck file from the given path.
def read_deck_file(temp_file_path):
    try:
        with open(temp_file_path, 'r') as file:
            deck_data = json.load(file)
            return deck_data
    except FileNotFoundError:
        print(f"Error: File not found: {temp_file_path}")
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON file: {e}")
    return None

# This function will filter the combo data to only include the relevant parts.
def filter_combo_data(combo_data):
    if "results" in combo_data and "included" in combo_data["results"]:
        filtered_data = {
            "count": combo_data.get("count"),
            "next": combo_data.get("next"),
            "previous": combo_data.get("previous"),
            "results": {
                "identity": combo_data["results"].get("identity"),
                "included": combo_data["results"].get("included")
            }
        }
        return filtered_data
    else:
        print("No 'included' key found in combo data results.")
        return None

# This function will write the flitered combo data to a JSON file
def write_combo_data(filtered_data, output_file_path):
    try:
        with open(output_file_path, 'w') as file:
            json.dump(filtered_data, file, indent=4)
        print(f"Combo data successfully written to {output_file_path}")
    except IOError as e:
        print(f"Error writing to file {output_file_path}: {e}")

if __name__ == "__main__":
    temp_file_path = "AI\\combos\\decks\\most_recent_deck.json"
    deck_data = read_deck_file(temp_file_path)

    if deck_data:
        if "commanders" in deck_data and deck_data["commanders"]:
            commander_name = deck_data["commanders"][0].replace(" ", "_")
            output_file_path = f"AI\\combos\\combo\\{commander_name}_combos.json"
        else:
            output_file_path = "AI\\combos\\combo\\default_combos.json"

        combo_data = fetch_combo_data(deck_data)
        if combo_data:
            filtered_data = filter_combo_data(combo_data)
            if filtered_data:
                write_combo_data(filtered_data, output_file_path)
        else:
            print("No combo data found.")
