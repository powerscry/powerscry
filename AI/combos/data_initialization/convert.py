import requests
import json
import os
import subprocess  # Import subprocess for running an external script

# This function will transrom a standard Moxfield URL to the corresponding API URL
def transform_url(moxfield_url):
    if "moxfield.com/decks" in moxfield_url:
        deck_id = moxfield_url.split('/')[-1]
        api_url = f"" #insert moxfieldlink here
        return api_url
    else:
        raise ValueError("Invalid Moxfield api URL format.")

# This function will fetch deck data from Moxfield API adn save it as a tempory JSON file
def fetch_deck_data(url, temp_file_path):
    response = requests.get(url)
    response.raise_for_status()  # Raise an error for bad responses
    deck_data = response.json()

    # Save the fetched data to a temporary file
    with open(temp_file_path, 'w') as temp_file:
        json.dump(deck_data, temp_file, indent=4)
    print(f"Fetched deck data saved to {temp_file_path}")

# The function will convert the temporary JSON file to the desired format and save the the new JSON files
def convert_deck(temp_file_path, output_dir):
    # Load the JSON data from the temporary file
    with open(temp_file_path, 'r') as file:
        deck_data = json.load(file)

    # Debug: Print loaded data structure
    print("Loaded deck data:", json.dumps(deck_data, indent=4))

    # Prepare the new JSON structure
    new_deck = {
        "commanders": [],
        "main": []
    }

    # Extract the commander(s)
    if 'main' in deck_data and 'name' in deck_data['main']:
        commander = deck_data['main']['name']
        new_deck['commanders'].append(commander)
    else:
        print("No commander found in the deck data.")

    # Add all other cards to the main deck
    if 'boards' in deck_data and 'mainboard' in deck_data['boards'] and 'cards' in deck_data['boards']['mainboard']:
        for card in deck_data['boards']['mainboard']['cards'].values():
            if 'card' in card and card['card']['name'] != commander:
                new_deck['main'].append(card['card']['name'])
    else:
        print("No mainboard cards found in the deck data.")

    # Sort the main deck cards alphabetically
    new_deck['main'].sort()

    # Debug: Print new deck structure before saving
    print("New deck structure:", json.dumps(new_deck, indent=4))

    # Create a valid filename from the first commander's name
    if new_deck['commanders']:
        commander_name = new_deck['commanders'][0].replace(' ', '_').replace(',', '').replace("'", "")  # Safe file naming
        output_file_path = os.path.join("AI//combos//decks", f"{commander_name}.json")
    else:
        output_file_path = os.path.join("AI//combos//decks", "default_deck.json")  # Fallback filename

    # Write the new deck structure to the output JSON file and the most recent deck file
    with open(output_file_path, 'w') as output_file, open(os.path.join("AI//combos//decks", "most_recent_deck.json"), 'w') as recent_file:
        json.dump(new_deck, output_file, indent=4)
        json.dump(new_deck, recent_file, indent=4)  # Duplicate the output in most_recent_deck.json

    print(f"Converted deck saved as {output_file_path}")
    print("Also saved as most_recent_deck.json")

def main():
    # Moxfield deck URL
    moxfield_url = "https://www.moxfield.com/decks/lFMFo_a_v0ufAk3fDsXveQ"
    
    # Transform the Moxfield URL to the API URL
    api_url = transform_url(moxfield_url)
    
    # Directory of the current script
    script_dir = os.path.dirname(os.path.abspath(__file__))

    # Path to the temporary JSON file
    temp_file_path = os.path.join(script_dir, "temp_deck.json")

    # Directory to save the output JSON file
    output_dir = script_dir

    # Fetch the deck data and save it as a temporary JSON file
    fetch_deck_data(api_url, temp_file_path)

    # Convert the temporary JSON file to the desired format and save the new JSON file
    convert_deck(temp_file_path, output_dir)

    print(f"Temporary file {temp_file_path} retained for inspection.")

    # Running the combos.py script
    subprocess.run(["python", os.path.join(script_dir, "combos.py")], check=True)

if __name__ == "__main__":
    main()
