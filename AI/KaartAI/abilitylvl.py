import sys
import os
import json

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

from AI.abilities.checklvl import predict_ability_rating

# Load JSON data
json_file_path = 'AI\KaartAI\InstantSorceries.json'
with open(json_file_path, 'r') as file:
    data = json.load(file)

# Extract abilities
abilities = []
for card in data.values():
    abilities.extend(card['abilities'])

# Example usage
if abilities:
    ability_key = abilities[0]  # Replace with logic to select the desired ability
    try:
        predicted_rating = predict_ability_rating(ability_key)
        print(f"The predicted rating for the ability '{ability_key}' is: {predicted_rating}")
    except ValueError as e:
        print(e)
else:
    print("No abilities found in the JSON data.")
