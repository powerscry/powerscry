
# README

## Overview

This project involves training neural network models to predict the strength and abilities of Magic: The Gathering (MTG) cards based on their features. The project includes various data files, Python scripts, and model weights for training and evaluating these models.

## Directory Structure

- `Abilities.json`: JSON file containing a list of abilities and their descriptions.
- `Abilitiesrandom.json`: JSON file containing random ability data for each ability.
- `ArtifactEnchantmentAI.py`: Python script for training and evaluating the model related to artifacts and enchantments.
- `randomfeatures.py`: Python script for handling random features in the dataset.
- `best_model.weights.h5`: HDF5 file containing the weights of the best-performing model.
- `final_model.weights.h5`: HDF5 file containing the weights of the final trained model.

## Dependencies

To run this project, you need to have the following Python packages installed:

- numpy
- json
- tensorflow
- scikit-learn
- matplotlib

You can install these dependencies using pip:

```bash
pip install numpy tensorflow scikit-learn matplotlib
```

## Project Files

### `Abilities.json`
A JSON file containing a list of MTG card abilities 

### `Abilitiesrandom.json`
A JSON file containing random data associated with each ability from `Abilities.json`.

### `ArtifactEnchantmentAI.py`
This script contains the code for training and evaluating a neural network model specifically for artifact, enchantment, sorcery and instant cards. It processes the card features, configures, trains, and evaluates the model.

### `randomfeatures.py`
This script contains functions to handle and manipulate random features within the dataset. It supports the processing of card abilities and integration into the model training.

### Model Weights
- `best_model.weights.h5`: Contains the weights of the best-performing model during training.
- `final_model.weights.h5`: Contains the weights of the final model after complete training.

## How to Use

1. **Prepare the Data**: Ensure all JSON files (`Abilities.json` and `Abilitiesrandom.json`) are in the correct directory and contain the data in the expected format.
2. **Run the Training Script**: Execute the `ArtifactEnchantmentAI.py` script to train the model and save the weights.
3. **Evaluate the Model**: Use the script to evaluate the model and plot the learning curves for training and validation accuracy and loss.
4. **Use the Model Weights**: Load the best model weights from `best_model.weights.h5` or the final model weights from `final_model.weights.h5` for re-evaluation or prediction tasks.

## Example Usage

```python
# Run the training script
python ArtifactEnchantmentAI.py

# The script will output the unique class labels, class distribution, shapes of X and y,
# validation loss, validation accuracy, and plots of the learning curves.
```

## Notes

- Adjust paths as necessary depending on your project structure.
- Ensure that the `predict_ability_rating` function is correctly implemented and imported from the appropriate module if needed.