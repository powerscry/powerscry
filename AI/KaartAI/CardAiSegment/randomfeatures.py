import numpy as np
import json

# Parameters for random data generation
num_samples = 1  # Number of samples per key
num_features = 194  # Number of features per sample
feature_range = (0, 3)  # Range of feature values

# Define the probabilities for each integer (biased towards 0)
probabilities = [0.5, 0.3, 0.15, 0.05]  # Probabilities for 0, 1, 2, and 3

# Function to generate biased random integer features
def generate_biased_integer_features(num_samples, num_features, probabilities):
    # Generate random integers with the specified probabilities
    features = np.random.choice(np.arange(feature_range[0], feature_range[1] + 1), 
                                size=(num_samples, num_features), 
                                p=probabilities)
    return features

# Read the existing JSON file
json_file_path = "AI\\abilities\\Abilitiesrandom.json"
with open(json_file_path, "r") as json_file:
    data = json.load(json_file)

# Print the original JSON data for debugging
print("Original JSON data:")
print(json.dumps(data, indent=4))

# Update all keys in the JSON
for key in data.keys():
    # Generate new random features for each key
    X = generate_biased_integer_features(num_samples, num_features, probabilities)
    # Convert features to comma-separated strings and update the JSON
    data[key] = [",".join(map(str, sample)) for sample in X]

# Print the updated JSON data for debugging
print("Updated JSON data:")
print(json.dumps(data, indent=4))

# Write the updated JSON back to the file
with open(json_file_path, "w") as json_file:
    json.dump(data, json_file, indent=4)

print(f"Random features with bias towards 0 generated and saved in {json_file_path}.")