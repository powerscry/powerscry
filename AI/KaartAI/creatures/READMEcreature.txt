##overview
The project involves training the neural network of an AI to predict the strength of MTG (Magic: the Gathering) cards based on their features. This model uses a dataset of MTG creatures to train a multi-class classification model to predict the card strength.


## Project Structure
`creature.py`: Python script for training and evaluating the model related to creatures.
`randomfeatures.py`: A Python script for handling random features in the dataset.
`CreatureLvl.txt`: A text file listing different types of creatures along with their attributes and abilities formatted in a structured manner.
`creatures.json`: A JSON file containing structured data about various creatures. Each creature's attributes and abilities are listed in a dictionary format.
`CreaturesRandom.json`: Similar to creatures.json, this JSON file includes random attributes associated with each creature.


##Dependencies
If you want to run this project first you need to install Python 3.x (we used 3.10.11)

Furthermore you need to have installed the following Python packages installed:

- numpy
- json
- tensorflow
- scikit-learn
- matplotlib

You can install these dependencies using pip install:

```bash
pip install numpy tensorflow scikit-learn matplotlib
```

## Project Files

###`creature.py`
    This script contains the code for training and evaluating a neural network model for creatures cards. It processes the card features, configures, trains, and evaluates the model.
###`randomfeatures.py`
    This script contains functions to handle and manipulate random features within the dataset. 
###`CreatureLvl.txt`
    A text file listing different types of creatures along with their attributes and abilities formatted in a structured manner. From this file the AI will use the target that is included
###`creatures.json`
    A JSON file containing structured data about various creatures.
###`CreaturesRandom.json`
    A JSON file containing random data associated with each creature from `creatures.json`.


### Model Weights
- `best_model.weights.h5`: Contains the weights of the best-performing model during training.
- `final_model.weights.h5`: Contains the weights of the final model after complete training.

##How to Use
1. **Preparing the data**: Ensure that the JSON files (`creatures.json` & `CreaturesRandom.json`)and the txt file (`CreatureLvl.txt`) are in the correct directory containing the data in the expected format.
2. **Run the Training Script**: Execute the `creature.py` script to train the model and save the weights
3. **Evaluate Model by look at the result**: Use the script you exceuted to evaluate the modeal and plot the learning curves for both training and validation accuracy and loss.
4. **Train it more by using the model wieghts**: Load the best model weights from the `best_model.weights.h5` file or the final mdoel weights from the `final_model.weights.h5` to re-evaluate in another training session

## Example useage run

```python
# run the training script `creature.py`
# The output of will give back 2 files
# file 1: final_model.weights.h5 - this contains the weights of the final model after completing the session
# file 2: best_model.weights.h5 - this contains the weights of the best-performing model during the training
# last but not least you get an overview in the form of an line graph with the validation loss, validation accuracy, and the plots of the learning curves.

## Important
- If you have a different path structure adjust the paths dependening on the structure you have