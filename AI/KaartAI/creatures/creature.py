# Creature cards have the following features.
# 1: Generic mana
# 2: W pips
# 3: U pips
# 4: B pips
# 5: R pips
# 6: G pips
# 7: Ability 1
# 8: Ability 2
# 9: Ability 3
# 10: Ability 4
# 11: Ability 5
# 12: Ability 6
# 13: Ability 7
# 14: Ability 8
# 15: Ability 9
# 16: Ability 10
# 17: the power a creature
# 18: the toughness of a creature

import os
import numpy as np
import json
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from tensorflow.keras.utils import to_categorical
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from collections import Counter
import sys

# sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..')))

# from AI.abilities.checklvl import predict_ability_rating

# This function is to process a list of the feature strings into a list of floats
def process_features_list(features_list):
    return [list(map(float, features.split(','))) for features in features_list]

# This function is to extract the rating from a line of text
def process_ratings_line(line):
    rating = int(line.strip().split(" - ")[-1])
    return rating

# This is the path to the JSON file containing the Creature features
CreatureLink = "AI\\KaartAI\\creatures\\CreaturesRandom.json"

# This will read and parse the JSON features files
with open(CreatureLink) as fileC:
    data = json.load(fileC)

# This will read and parse the ratings file
with open("AI\\KaartAI\\creatures\\CreatureLvl.txt") as fileR:
    rating_lines = fileR.readlines()

# This will extract the features and ratings
X_train = []
for keay, features_list in data.items():
    X_train.extend(process_features_list(features_list)) # this will progress each feature in a list and extend it to X_train
    y_train = [process_ratings_line(line) for line in rating_lines] # This will process each rating line into an integer

# This will adjust the class labels to be in the range from [0, 9]
y_train = [label - 1 for label in y_train]

# This will check for the unique class labels
unique_labels = np.unique(y_train)
print(f"Unique class labels: {unique_labels}")

# This will check for the class distribution
class_counts = Counter(y_train)
print("Class distribution:", class_counts)

# This will convert to our X_train & Y_train to numpy arrays
X = np.array(X_train)
y = np.array(y_train)

# One-hot encode the labels for multi-class classification
y = to_categorical(y, num_classes=10)

# Debugging: Print shapes to verify
print(f"Shape of X: {X.shape}")
print(f"Shape of y: {y.shape}")

# Split data into training and validation sets without stratification
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

# Feature scaling
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)  # Fit and transform the training data
X_val = scaler.transform(X_val)  # Transform the validation data

# Simplified Model Configuration
model = Sequential([
    Dense(units=128, activation="relu", input_shape=(X_train.shape[1],)),
    Dropout(0.5),
    Dense(units=64, activation="relu"),
    Dropout(0.5),
    Dense(units=32, activation="relu"),
    Dropout(0.5),
    Dense(units=10, activation="softmax")  # Output layer for 10 classes
])

# Model Compilation
model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),  # Adjusted learning rate
    loss='categorical_crossentropy',  # Use categorical cross-entropy for multi-class classification
    metrics=['accuracy']  # Monitor accuracy
)

# Learning rate scheduler
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=1e-6)

# Early stopping with increased patience
early_stopping = EarlyStopping(monitor='val_loss', patience=20, restore_best_weights=True)

# Model Checkpoint Callback to save the best model
checkpoint = ModelCheckpoint('AI\\KaartAI\\creatures\\best_model.weights.h5', monitor='val_loss', save_best_only=True, save_weights_only=True)

# Model Training with Early Stopping and Checkpoint
history = model.fit(
    X_train, y_train,
    epochs=100,
    batch_size=64,  # Adjusted batch size
    validation_data=(X_val, y_val),
    shuffle=True,  # Ensure data is shuffled each epoch
    callbacks=[early_stopping, checkpoint, reduce_lr]
)

# Save the final model weights
model.save_weights('AI\\KaartAI\\creatures\\final_model.weights.h5')

# Model Evaluation
loss, accuracy = model.evaluate(X_val, y_val)

print(f"Validation Loss: {loss}")
print(f"Validation Accuracy: {accuracy}")

# Plot learning curves
plt.figure(figsize=(12, 6))

# Plot training & validation accuracy values
plt.subplot(1, 2, 1)
plt.plot(history.history['accuracy'], label='Train Accuracy')
plt.plot(history.history['val_accuracy'], label='Validation Accuracy')
plt.title('Model accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.legend(loc='upper left')
plt.grid(True)

# Plot training & validation loss values
plt.subplot(1, 2, 2)
plt.plot(history.history['loss'], label='Train Loss')
plt.plot(history.history['val_loss'], label='Validation Loss')
plt.title('Model loss')
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.legend(loc='upper left')
plt.grid(True)

plt.tight_layout()
plt.show()