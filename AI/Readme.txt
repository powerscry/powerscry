This file will explain how our files are structured, there will also be more in-depth descriptions in most folders explaining what the function of the files in the folder is and how they work.
Explanations of installations and steps before running will be in the folder

File structure and explanation of functions 

AI
|__pycache__
|__.vscode
|__abilities                "This will determine the strength of an ability using the formulav3.py file"
   |__AI_formula_old        "These are early non-functional versions of the AI formula"
   |__segment               "This is to convert the information from Moxfield to data useable by our AI"
|__combos                   
   |__Actual AI             "This is the AI for combos, it will determine the strength of a combo using data retrieved from CommanderSpellbook"
   |__combo                 "Combo contains the featurelist we use for the AI and is also where the combodata of a deck is placed"
   |__Data_initialization   "This folder is used to convert the information from CommanderSpellbook into useable data for the AI"
   |__decks                 "This is a storage for all decks currently converted to be able to be sent to the CommanderSpellbook API"
|__KaartAI                  "This is used to calculate the strength of of individual cards"
   |__CardAiSegment         "This can be used to determine the  for all cards"
   |__creatures             "This determines the strength of creatures. It can be replaced by the CardAiSegment file, but is mostly here"
|__synergies                "This is currently hardcoded to extract the amount of synergies in a deck. We would like to use an Nearest neighbour AI in the future"
